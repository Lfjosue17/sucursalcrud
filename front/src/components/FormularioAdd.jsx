import React, { useState } from "react";
import { v4 as uuid } from "uuid";

const FormularioAdd = ({ dispatch }) => {
  const [data, setData] = useState({ nombre: "", administrador:"", telefono: "", direccion: "",fax: "", pedidos: "", ventas: "", compras: "", inventario: "", gastos: "", reportes: "", usuarios: "", configuracion: "", });

  const { nombre, administrador, telefono, direccion, fax, pedidos } = data;

  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  const actionAdd = {
    type: "add",
    payload: {
      id: uuid(),
      nombre,
      administrador,
      telefono,
      direccion,
      fax,
      pedidos,
    },
  };

  const handleAdd = () => {
    dispatch(actionAdd);
  };

  return (
    <>
      <div className="container">
        <label className="mx-1 d-grid gap-2">
          Nombre:{" "}
          <input
            onChange={handleChange}
            name="nombre"
            value={nombre}
            type="text"
            className="form-control"
            autoComplete="off"
            required
          />
        </label>
        <label className="mx-1 d-grid gap-2">
          Administrador:{" "}
          <input
            onChange={handleChange}
            value={administrador}
            name="administrador"
            type="text"
            className="form-control"
            autoComplete="off"
            required
          />
        </label>
        <label className="mx-1 d-grid gap-2">
          Telefono:{" "}
          <input
            onChange={handleChange}
            value={telefono}
            name="telefono"
            type="text"
            className="form-control"
            autoComplete="off"
            required
          />
        </label>
        <label className="mx-1 d-grid gap-2">
          Direccion:{" "}
          <input
            onChange={handleChange}
            value={direccion}
            name="direccion"
            type="text"
            className="form-control"
            autoComplete="off"
            required
          />
        </label>
        <label className="mx-1 d-grid gap-2">
          Fax:{" "}
          <input
            onChange={handleChange}
            value={fax}
            name="fax"
            type="text"
            className="form-control"
            autoComplete="off"
          />
        </label>
        <label className="mx-1 d-grid gap-2">
          Pedidos:{" "}
          <input
            onChange={handleChange}
            value={pedidos}
            name="pedidos"
            type="text"
            className="form-control"
            autoComplete="off"
          />
        </label>
        <div className="mx-1 d-grid gap-2">
          <button onClick={handleAdd} className="btn btn-info mt-2">
            Agregar
          </button>
        </div>
      </div>
    </>
  );
};

export default FormularioAdd;
