import React from "react";

const TablaSucursales = ({ Sucursales = [], dispatch }) => {
  const handleDelete = (id) => {
    const deleteAction = {
      type: "delete",
      payload: id,
    };

    dispatch(deleteAction);
  };

  return (
    <table className="table">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Administrador</th>
          <th>Telefono</th>
          <th>Direccion</th>
          <th>Fax</th>
          <th>Pedidos</th>
          <th>Acción</th>
        </tr>
      </thead>
      <tbody>
        {Sucursales.map((Sucursal) => {
          const finalId = Sucursal.id.split("-");

          return (
            <tr key={Sucursal.id}>
              <th>{finalId[0]}</th>
              <td>{Sucursal.nombre}</td>
              <td>{Sucursal.administrador}</td>
              <td>{Sucursal.telefono}</td>
              <td>{Sucursal.direccion}</td>
              <td>{Sucursal.fax}</td>
              <td>{Sucursal.pedidos}</td>
              <td>
                <button
                  onClick={() => handleDelete(Sucursal.id)}
                  className="btn btn-danger"
                >
                  Eliminar
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default TablaSucursales;
