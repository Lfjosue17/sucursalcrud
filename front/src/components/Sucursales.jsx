import React, { useEffect, useReducer, useState } from "react";
import { SucursalesReducer } from "../reducers/SucursalesReducer";
import FormularioAdd from "./FormularioAdd";
import TablaSucursales from "./TablaSucursales";

const init = () => {
  const Sucursales = localStorage.getItem("Sucursales");

  return Sucursales ? JSON.parse(Sucursales) : [];
};

const Sucursales = () => {
  const [state, dispatch] = useReducer(SucursalesReducer, [], init);

  useEffect(() => {
    localStorage.setItem("Sucursales", JSON.stringify(state));
  }, [state]);

  const [formView, setFormView] = useState(false);

  return (
    <div className="container mt-3">
      <button
        onClick={() => setFormView(!formView)}
        className="btn btn-success"
      >
        {!formView ? "+ Agregar Sucursal" : "- Cerrar Formulario"}
      </button>
      {formView && <FormularioAdd dispatch={dispatch} />}

      <TablaSucursales Sucursales={state} dispatch={dispatch} />
    </div>
  );
};

export default Sucursales;
