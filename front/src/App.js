import React from "react";
import Sucursales from "./components/Sucursales";
import Header from "./components/Header";

const App = () => {
  return (
    <>
      <Header />
      <Sucursales />
    </>
  );
};

export default App;
