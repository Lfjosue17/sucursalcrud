package sucursaeslCRUD.models;


import java.sql.Date;

import javax.persistence.*;

@Entity
@Table(name = "Sucursales")
public class sucursalModels {

    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "SERIAL")
    private Integer id_sucursal;
    @Column(columnDefinition = "VARCHAR NOT NULL", unique = true)
    private String sucursal;
    @Column(columnDefinition = "VARCHAR NOT NULL")
    private String administrador;
    @Column(columnDefinition = "VARCHAR NOT NULL")
    private String telefono;
    @Column(columnDefinition = "VARCHAR NOT NULL")
    private String direccion;
    @Column(columnDefinition = "VARCHAR")
    private String fax;
    @Column(columnDefinition = "NUMERIC")
    private Float pedidos_mes;
    @Column(columnDefinition = "TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL", updatable =false, insertable=false)
    private Date fecha_creacion;
    @Column(columnDefinition = "TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL", updatable =false, insertable=false)
    private Date fecha_actualizacion;
    @Column(columnDefinition = "BOOLEAN DEFAULT true")
    private Boolean activo;



    public Integer getId() {
        return id_sucursal;
    }

    public void setId(Integer id) {
        this.id_sucursal = id;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getAdministrador() {
        return administrador;
    }

    public void setAdministrador(String administrador) {
        this.administrador = administrador;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Float getPedidosMes() {
        return pedidos_mes;
    }

    public void setPedidosMes(Float pedidos_mes) {
        this.pedidos_mes = pedidos_mes;
    }

    public Date getFechaRegistro(){
        return fecha_creacion;
    }

    public void setFechaRegistro(Date fecha_creacion){
        this.fecha_creacion = fecha_creacion;
    }

    public Date getFechaAct(){
        return fecha_actualizacion;
    }

    public void setFechaAct(Date fecha_actualizacion){
        this.fecha_actualizacion = fecha_actualizacion;
    }

    public Boolean getActivo(){
        return activo;
    }

    public void setActivo(Boolean activo){
        this.activo = activo;
    }
}
