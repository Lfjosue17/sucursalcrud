package sucursaeslCRUD.services;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sucursaeslCRUD.repositories.sucursalRepository;
import sucursaeslCRUD.models.sucursalModels;

@Service
public class sucursalService {
    @Autowired
    sucursalRepository sucursalRepository;
    
    public ArrayList<sucursalModels> obtenerSucursales(){
        return (ArrayList<sucursalModels>) sucursalRepository.findAll();
    }

    public sucursalModels guardarSucursal(sucursalModels sucursal){
        return sucursalRepository.save(sucursal);
    }

    public Optional<sucursalModels> obtenerPorId(Integer id){
        return sucursalRepository.findById(id);
    }

    //servicio buscar sucusales activas
    public ArrayList<sucursalModels> obtenerSucursalesActivas(){
        return sucursalRepository.findByactivo(true);
    }
    
    public boolean eliminarSucursal(Integer id) {
        try{
            sucursalRepository.deleteById(id);
            return true;
        }catch(Exception err){
            return false;
        }
    }
}
