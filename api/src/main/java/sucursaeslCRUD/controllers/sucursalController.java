package sucursaeslCRUD.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import sucursaeslCRUD.services.sucursalService;
import sucursaeslCRUD.models.sucursalModels;

@RestController
@RequestMapping("/api")
public class sucursalController {
    @Autowired
    sucursalService sucursalService;

    @GetMapping(path = "/getSucursales")
    public ArrayList<sucursalModels> obtenerSucursales(){
        return sucursalService.obtenerSucursales();
    }

    @PostMapping(path = "/setSucursal")
    public sucursalModels guardarSucursal(@RequestBody sucursalModels sucursal){
        return this.sucursalService.guardarSucursal(sucursal);
    }

    @GetMapping( path = "/getSucursalById/{id}")
    public Optional<sucursalModels> obtenerSucursalPorId(@PathVariable("id") Integer id) {
        return this.sucursalService.obtenerPorId(id);
    }

    @GetMapping( path = "/getSucursalesActivas")
    public ArrayList<sucursalModels> obtenerSucursalesActivas() {
        return this.sucursalService.obtenerSucursalesActivas();
    }
}
