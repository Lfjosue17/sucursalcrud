package sucursaeslCRUD.repositories;

import sucursaeslCRUD.models.sucursalModels;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface sucursalRepository extends CrudRepository<sucursalModels, Integer> {
    public abstract ArrayList<sucursalModels> findByactivo(boolean activo);
}
