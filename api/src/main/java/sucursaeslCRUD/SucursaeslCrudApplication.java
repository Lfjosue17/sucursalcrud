package sucursaeslCRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SucursaeslCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SucursaeslCrudApplication.class, args);
	}

}
